﻿using System;
using System.Collections.Generic;
using System.Text;
using TagJobs.Core.Domain;

namespace TagJobs.Core.DataInterfaces
{
    public interface IProfessionalRepository : IGenericRepository<Professional>
    {
    }
}
