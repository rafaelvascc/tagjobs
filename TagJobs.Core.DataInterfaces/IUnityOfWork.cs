﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TagJobs.Core.DataInterfaces
{
    public interface IUnityOfWork
    {
        IProfessionalRepository ProfessionalRepository { get; }

        void Begin();
        void Commit();
        void Rollback();
    }
}
