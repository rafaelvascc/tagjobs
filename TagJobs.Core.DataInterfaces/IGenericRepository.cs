﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TagJobs.Core.Domain;

namespace TagJobs.Core.DataInterfaces
{
    public interface IGenericRepository<T> where T : AbstractEntity
    {
        IList<T> GetAll();
        T GetById(Guid id);
        T Insert(T entity);
        T Update(T entity);
        IList<T> InsertMany(IList<T> entity);
        IList<T> UpdateMany(IList<T> entity);
        void Delete(T entity);
        void Delete(Guid id);
        void DeleteMany(IList<T> entity);
        void DeleteMany(IList<Guid> ids);

        Task<IList<T>> GetAllAsync();
        Task<T> GetByIdAsync(Guid id);
        Task<T> InsertAsync(T entity);
        Task<T> UpdateAsync(T entity);
        Task<IList<T>> InsertManyAsync(IList<T> entity);
        Task<IList<T>> UpdateManyAsync(IList<T> entity);
        Task DeleteAsync(T entity);
        Task DeleteAsync(Guid id);
        Task DeleteManyAsync(IList<T> Entity);
        Task DeleteManyAsync(IList<Guid> ids);
    }
}
