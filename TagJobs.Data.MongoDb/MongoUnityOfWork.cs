﻿using System;
using System.Collections.Generic;
using System.Text;
using TagJobs.Core.DataInterfaces;

namespace TagJobs.Data.MongoDb
{
    public class MongoUnityOfWork : IUnityOfWork
    {
        IProfessionalRepository professionalRespository;

        public MongoUnityOfWork(IProfessionalRepository professionalRespository)
        {
            this.professionalRespository = professionalRespository;
        }

        public IProfessionalRepository ProfessionalRepository { get => professionalRespository; }

        public void Begin()
        {
        }

        public void Commit()
        {
        }

        public void Rollback()
        {
        }
    }
}
