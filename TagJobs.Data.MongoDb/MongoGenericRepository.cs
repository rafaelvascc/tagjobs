﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using TagJobs.Core.DataInterfaces;
using System.Threading.Tasks;
using TagJobs.Core.Domain;
using System.Linq;
using MongoDB.Bson;

namespace TagJobs.Data.MongoDb
{
    public class MongoGenericRepository<T> : IGenericRepository<T> where T : AbstractEntity
    {
        protected IMongoCollection<T> collection;

        public MongoGenericRepository(IMongoDatabase database)
        {
            database.GetCollection<T>(typeof(T).Name.ToLower());
        }

        public void Delete(T entity)
        {
            collection.DeleteOne<T>(e => e.Id == entity.Id);
        }

        public void Delete(Guid id)
        {
            collection.DeleteOneAsync<T>(e => e.Id == id);
        }

        public async Task DeleteAsync(T entity)
        {
            await collection.DeleteOneAsync<T>(e => e.Id == entity.Id);
        }

        public async Task DeleteAsync(Guid id)
        {
            await collection.DeleteOneAsync<T>(e => e.Id == id);
        }

        public void DeleteMany(IList<T> entities)
        {
            var ids = entities.Select(e => e.Id).ToList();
            collection.DeleteMany<T>(e => ids.Contains(e.Id));
        }

        public void DeleteMany(IList<Guid> ids)
        {
            collection.DeleteMany<T>(e => ids.Contains(e.Id));
        }

        public async Task DeleteManyAsync(IList<T> entities)
        {
            var ids = entities.Select(e => e.Id).ToList();
            await collection.DeleteManyAsync<T>(e => ids.Contains(e.Id));
        }

        public async Task DeleteManyAsync(IList<Guid> ids)
        {
            await collection.DeleteManyAsync<T>(e => ids.Contains(e.Id));
        }

        public IList<T> GetAll()
        {
            return collection.Find<T>(Builders<T>.Filter.Empty).ToList();
        }

        public async Task<IList<T>> GetAllAsync()
        {
            return await collection.Find<T>(Builders<T>.Filter.Empty).ToListAsync<T>();
        }

        public T GetById(Guid id)
        {
            return collection.Find(e => e.Id == id).SingleOrDefault();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await collection.Find(e => e.Id == id).SingleOrDefaultAsync();
        }

        public T Insert(T entity)
        {
            collection.InsertOne(entity);
            return entity;
        }

        public async Task<T> InsertAsync(T entity)
        {
            await collection.InsertOneAsync(entity);
            return entity;
        }

        public IList<T> InsertMany(IList<T> entities)
        {
            collection.InsertMany(entities);
            return entities;
        }

        public async Task<IList<T>> InsertManyAsync(IList<T> entities)
        {
            await collection.InsertManyAsync(entities);
            return entities;
        }

        public T Update(T entity)
        {
            return collection.FindOneAndUpdate<T, T>(e => e.Id == entity.Id, entity.ToBsonDocument(), new FindOneAndUpdateOptions<T>() { ReturnDocument = ReturnDocument.After });
        }

        public async Task<T> UpdateAsync(T entity)
        {
            return await collection.FindOneAndUpdateAsync<T, T>(e => e.Id == entity.Id, entity.ToBsonDocument(), new FindOneAndUpdateOptions<T>() { ReturnDocument = ReturnDocument.After });
        }

        public IList<T> UpdateMany(IList<T> entities)
        {
            throw new NotImplementedException();
        }

        public Task<IList<T>> UpdateManyAsync(IList<T> entities)
        {
            throw new NotImplementedException();
        }
    }
}
