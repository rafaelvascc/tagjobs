﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using TagJobs.Core.DataInterfaces;
using TagJobs.Core.Domain;

namespace TagJobs.Data.MongoDb
{
    public class MongoProfessionalRepository : MongoGenericRepository<Professional>, IProfessionalRepository
    {
        public MongoProfessionalRepository(IMongoDatabase database) : base(database)
        {
        }
    }
}
