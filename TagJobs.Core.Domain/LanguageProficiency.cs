﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace TagJobs.Core.Domain
{
    public class LanguageProficiency : AbstractEntity
    {
        public string LanguageIsoCode { set; get; }
        public LanguageProficiencyLevel OverallLevel { set; get; }
        public LanguageProficiencyLevel ListeningLevel { set; get; }
        public LanguageProficiencyLevel SpeakingLevel { set; get; }
        public LanguageProficiencyLevel ReadingLevel { set; get; }
        public LanguageProficiencyLevel WritingLevel { set; get; }
    }
}
