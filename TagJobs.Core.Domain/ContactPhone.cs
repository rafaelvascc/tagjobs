﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TagJobs.Core.Domain
{
    public class ContactPhone : AbstractEntity
    {
        public string PhoneNumber { set; get; }
        public ContactType Type { set; get; }
    }
}
