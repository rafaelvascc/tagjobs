﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TagJobs.Core.Domain
{
    public class Professional : AbstractEntity
    {
        public string FirstName { set; get; }
        public string MiddleName { set; get; }
        public string LastName { set; get; }
        public string SexualIdentification { set; get; }        
        public DateTime BirthDate { set; get; }
        public Address ResidentialAddress { set; get; }
        public ISet<ContactPhone> PhoneNumbers { set; get; } = new HashSet<ContactPhone>();
        public ISet<ContactEmail> Emails { set; get; } = new HashSet<ContactEmail>();
        public ISet<Curriculum> Curriculums { set; get; } = new HashSet<Curriculum>();
        public ISet<LanguageProficiency> Languages { set; get; } = new HashSet<LanguageProficiency>();
        public ISet<Tag> Tags { set; get; }
    }
}
