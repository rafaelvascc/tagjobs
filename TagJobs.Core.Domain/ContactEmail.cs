﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TagJobs.Core.Domain
{
    public class ContactEmail : AbstractEntity
    {
        public string Email { set; get; }
        public ContactType Type { set; get; }
    }
}
