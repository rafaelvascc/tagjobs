﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TagJobs.Core.Domain
{
    public class ProfessionalExperience : AbstractEntity
    {
        public Guid Id { set; get; }
        public Boolean IsCurrentJob { set; get; }
        public DateTime DateStart { set; get; }
        public DateTime DateEnd { set; get; }
        public string Position { set; get; }
        public string CompanyName { set; get; }
        public string Location { set; get; }
        public string Description { set; get; }
    }
}
