﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TagJobs.Core.Domain
{
    public class AbstractEntity
    {
        public Guid Id { set; get; }
        public DateTime DateCreated { set; get; }
        public DateTime LastMofidied { set; get; }
    }
}
