﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TagJobs.Core.Domain
{
    public enum ContactType
    {
        Residential,
        Mobile,
        Business,
        Other
    }
}
