﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TagJobs.Core.Domain
{
    public class Tag : AbstractEntity
    {
        public string Value { set; get; }
    }
}
