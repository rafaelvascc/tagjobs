﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TagJobs.Core.Domain
{
    public enum AcademicExperienceLevel
    {
        PrimarySchool,
        SecondaySchool,
        Associate,
        Bachelor,
        Masters,
        Doctor,
        PostDoctor,
        OtherTechnicalDegree,
        OtherCollegeOrUniversityGraduation,
        OtherPostGraduation
    }
}
