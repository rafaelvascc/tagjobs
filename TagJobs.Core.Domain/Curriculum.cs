﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TagJobs.Core.Domain
{
    public class Curriculum : AbstractEntity
    {
        public string Language { set; get; } 
        public string CoverLetter { set; get; }
        public ISet<AcademicExperience> AcademicExperiences { set; get; }
        public ISet<ProfessionalExperience> ProfessionalExperiences { set; get; }
        public ISet<Course> Courses { set; get; }
    }
}
