﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TagJobs.Core.Domain
{
    public class Course : AbstractEntity
    {
        public string Institution { set; get; }
        public string CourseName { set; get; }
        public DateTime StartDate { set; get; }
        public DateTime EndDate { set; get; }
        public CourseStatus Status { set; get; }
        public string AdditionalDescription { set; get; }
    }
}
