﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TagJobs.Core.Domain
{
    public class Address : AbstractEntity
    {
        public string Street { set; get; }
        public string Number { set; get; }
        public string ZipCode { set; get; }
        public string AdittionalInfo { set; get; }
        public string City { set; get; }
        public string StateOrProvince { set; get; }
        public string Country { set; get; }
    }
}
